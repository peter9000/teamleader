<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "Hallo, gebruik de endpoint /discount om dit te testen. Dit endpoint aanvaardt een json body.";
});

Route::post('/discount', 'DiscountController@calculateDiscount');

