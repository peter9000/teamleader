# Teamleader Coding Test 1: Discounts

## Author

Peter De Bouvere, pdebouvere@gmail.com

## Basic information

### App usage

The app is located on `http://188.166.42.75`. A POST request with the JSON data should be made to 
`http://188.166.42.75/discount`

### Response

The endpoint returns a basic JSON response, with the order id, the order total before the discounts, the order total 
after the discounts and a list of applied discount descriptions. For example:

```
{
	"order-id": 2,
	"order-total-before-discount": "24.95",
	"order-total-after-discount": "22.46",
	"discounts": [
		"Discount of 10% on the total order, because the revenue is over 1000 EUR"
	]
}
```

## Creating additional discount rules

All discount rules are located in the `App\DiscountRules` namespace, and must adhere to the `DiscountRuleInterface`
interface.

The `apply()` method takes the `Order` object, and returns an `Amount` object. If the amount is zero, the discount is 
not applied.

Example rule:

```
<?php

namespace App\DiscountRules;

use App\ObjectValues\Amount;
use App\Order;

final class TenPercentIfRevenueOver1000DiscountRule implements DiscountRuleInterface
{
    /**
     * Applies the discount rule to an order.
     *
     * @param Order $order
     *
     * @return Amount
     */
    public function apply(Order $order): Amount
    {
        $amount = new Amount(0);

        if ($order->getCustomer()->getRevenue()->getAmount() > 100000) {
            $total = $order->getTotalAfterDiscount()->getAmount();
            $amount = new Amount($total / 10);
        }

        return $amount;
    }

    /**
     * Provides a helpful description of this rule.
     *
     * @return string
     */
    public function description(): string
    {
        return "Discount of 10% on the total order, because the revenue is over 1000 EUR";
    }
}
```

**Important!** The rule must be added to the rules in the `Order` model, in the `applyDiscounts()` method. For example:

```
private function applyDiscounts()
{
    $rules = [
        new SwitchesFivePlusOneFreeDiscountRule(), 
        new TwentyFivePercentOnCheapestToolDiscountRule(),
        new TenPercentIfRevenueOver1000DiscountRule(),
    ];
    (...)
```

## Extended information

I created a small Laravel application, with only one actual route (`/discount`). The products and customers are loaded 
by their respective respository (see `App\Repositories`). The heavy lifting is done by the base class `JsonRepository`.

Because I like to keep objects immutable, no setters were used, and the constructor of each model creates a final,
immutable instance.

There is no authentication provided, that was out of scope for this assignment. If a faulty JSON body is posted, the 
default exceptions will be thrown, there is no validation.