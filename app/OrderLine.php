<?php

namespace App;

use App\ObjectValues\Amount;
use App\Repositories\ProductRepository;

/**
 * Class OrderLine
 *
 * @package App
 */
class OrderLine
{
    /* @var Product $product */
    private $product;

    /* @var int $quantity */
    private $quantity;

    /* @var Amount $unitPrice */
    private $unitPrice;

    /* @var Amount $linePrice */
    private $linePrice;

    /**
     * OrderLine constructor.
     *
     * @param $data
     * @param ProductRepository $productRepository
     */
    public function __construct($data, ProductRepository $productRepository)
    {
        $this->product = $productRepository->get($data['product-id']);
        $this->quantity = (int)$data['quantity'];
        $this->unitPrice = new Amount($data['unit-price']);
        $this->linePrice = new Amount($data['total']);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return Amount
     */
    public function getUnitPrice(): Amount
    {
        return $this->unitPrice;
    }

    /**
     * @return Amount
     */
    public function getLinePrice(): Amount
    {
        return $this->linePrice;
    }
}
