<?php

namespace App\Http\Controllers;

use App\Order;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    /* @var CustomerRepository $customerRepository */
    private $customerRepository;

    /* @var ProductRepository $productRepository */
    private $productRepository;

    /**
     * DiscountController constructor.
     *
     * @param CustomerRepository $customerRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(CustomerRepository $customerRepository, ProductRepository $productRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Creates an order, which automatically applies the discount rules provided in the Order class.
     *
     * @param Request $request
     *
     * @return array
     */
    public function calculateDiscount(Request $request): array
    {
        $order = new Order($request->all(), $this->customerRepository, $this->productRepository);

        return [
            'order-id' => $order->getId(),
            'order-total-before-discount' => $order->getTotalBeforeDiscount()->getFormattedAmount(),
            'order-total-after-discount' => $order->getTotalAfterDiscount()->getFormattedAmount(),
            'discounts' => $order->getDiscountDescriptions(),
        ];
    }
}
