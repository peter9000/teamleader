<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

abstract class JsonRepository extends Collection
{
    public function __construct()
    {
        parent::__construct();

        $file = $this->getFile();
        $model = $this->getModel();

        $path = resource_path() . '/data/' . $file;
        $json = json_decode(file_get_contents($path), true);

        foreach ($json as $jsonItem) {
            $item = new $model($jsonItem);
            $this->put($item->getId(), $item);
        }
    }
}
