<?php

namespace App\Repositories;

interface JsonRepositoryInterface
{
    public function getModel(): string;
    public function getFile(): string;
}