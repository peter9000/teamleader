<?php

namespace App\Repositories;

use App\Product;

class ProductRepository extends JsonRepository implements JsonRepositoryInterface
{
    /**
     * Returns the model classname this repository should generate.
     *
     * @return string
     */
    public function getModel(): string
    {
        return Product::class;
    }

    /**
     * Returns the filename of the json file inside the resources/data folder.
     *
     * @return string
     */
    public function getFile(): string
    {
        return 'products.json';
    }
}