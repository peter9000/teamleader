<?php

namespace App\Repositories;

use App\Customer;

/**
 * Class CustomerRepository
 *
 * @package App\Repositories
 */
class CustomerRepository extends JsonRepository implements JsonRepositoryInterface
{
    /**
     * Returns the model classname this repository should generate.
     *
     * @return string
     */
    public function getModel(): string
    {
        return Customer::class;
    }

    /**
     * Returns the filename of the json file inside the resources/data folder.
     *
     * @return string
     */
    public function getFile(): string
    {
        return 'customers.json';
    }
}