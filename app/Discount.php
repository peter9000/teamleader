<?php

namespace App;

use App\DiscountRules\DiscountRuleInterface;
use App\ObjectValues\Amount;

/**
 * Class Discount
 *
 * @package App
 */
class Discount
{
    /* @var DiscountRuleInterface $discountRule */
    private $discountRule;

    /* @var Order $order */
    private $order;

    /* @var Amount $discountAmount */
    private $discountAmount;

    /* @var string $description */
    private $description;

    /**
     * Discount constructor.
     *
     * @param DiscountRuleInterface $discountRule
     * @param Order $order
     */
    public function __construct(DiscountRuleInterface $discountRule, Order $order)
    {
        $this->discountRule = $discountRule;
        $this->order = $order;

        $this->discountAmount = $discountRule->apply($order);
        $this->description = $discountRule->description();
    }

    /**
     * Checks if this discount applies to the order.
     *
     * @return bool
     */
    public function doesApply(): bool
    {
        return $this->discountAmount->getAmount() > 0;
    }

    /**
     * @return Amount
     */
    public function getDiscountAmount(): Amount
    {
        return $this->discountAmount;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
