<?php

namespace App;

use App\ObjectValues\Amount;
use Carbon\Carbon;

/**
 * Class Customer
 *
 * @package App
 */
class Customer
{
    /* @var int $id */
    private $id;

    /* @var string $name */
    private $name;

    /* @var Carbon $since */
    private $since;

    /* @var Amount $revenue */
    private $revenue;

    /**
     * Customer constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->id = (int)$data['id'];
        $this->name = $data['name'];
        $this->since = Carbon::createFromFormat('Y-m-d', $data['since']);
        $this->revenue = new Amount($data['revenue']);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Amount
     */
    public function getRevenue()
    {
        return $this->revenue;
    }
}
