<?php

namespace App;

use App\DiscountRules\SwitchesFivePlusOneFreeDiscountRule;
use App\DiscountRules\TenPercentIfRevenueOver1000DiscountRule;
use App\DiscountRules\TwentyFivePercentOnCheapestToolDiscountRule;
use App\ObjectValues\Amount;
use App\Repositories\CustomerRepository;
use App\Repositories\ProductRepository;
use Illuminate\Support\Collection;

/**
 * Class Order
 *
 * @package App
 */
class Order
{
    /* @var int $id */
    private $id;

    /* @var Customer $customer */
    private $customer;

    /* @var Collection $orderLines */
    private $orderLines;

    /* @var Amount $totalBeforeDiscount */
    private $totalBeforeDiscount;

    /* @var Amount $totalAfterDiscount */
    private $totalAfterDiscount;

    /* @var Collection $discounts */
    private $discounts;

    /**
     * Order constructor.
     *
     * @param $data
     * @param CustomerRepository $customerRepository
     * @param ProductRepository $productRepository
     */
    public function __construct($data, CustomerRepository $customerRepository, ProductRepository $productRepository)
    {
        $this->id = $data['id'];
        $this->customer = $customerRepository->get((int)$data['customer-id']);
        $this->orderLines = new Collection();
        foreach ($data['items'] as $lineData) {
            $this->orderLines->push(new OrderLine($lineData, $productRepository));
        }
        $this->totalBeforeDiscount = new Amount($data['total']);
        $this->totalAfterDiscount = new Amount($data['total']);

        $this->applyDiscounts();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return Collection
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    /**
     * @return Amount
     */
    public function getTotalBeforeDiscount(): Amount
    {
        return $this->totalBeforeDiscount;
    }

    /**
     * @return Amount
     */
    public function getTotalAfterDiscount(): Amount
    {
        return $this->totalAfterDiscount;
    }

    /**
     * @return array
     */
    public function getDiscountDescriptions(): array
    {
        $descriptions = [];

        foreach ($this->discounts as $discount) {
            $descriptions[] = $discount->getDescription();
        }

        return $descriptions;
    }

    /**
     * Iterates through the provided discount rules, and applies them. Calculates a new total amount of the order.
     */
    private function applyDiscounts()
    {
        $rules = [
            new SwitchesFivePlusOneFreeDiscountRule(),
            new TwentyFivePercentOnCheapestToolDiscountRule(),
            new TenPercentIfRevenueOver1000DiscountRule(),
        ];

        $this->discounts = new Collection();
        foreach ($rules as $rule) {
            $discount = new Discount($rule, $this);
            if ($discount->doesApply()) {
                $this->discounts->push($discount);
                $this->calculateNewTotal($discount->getDiscountAmount());
            }
        }

    }

    /**
     * Calculates the new total amount for this order for the given discount.
     *
     * @param Amount $discountAmount
     */
    private function calculateNewTotal(Amount $discountAmount)
    {
        $previousAmount = $this->totalAfterDiscount->getAmount();
        $subtract = $discountAmount->getAmount();
        $this->totalAfterDiscount = new Amount($previousAmount - $subtract);
    }
}
