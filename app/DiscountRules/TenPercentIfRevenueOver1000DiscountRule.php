<?php

namespace App\DiscountRules;

use App\ObjectValues\Amount;
use App\Order;

final class TenPercentIfRevenueOver1000DiscountRule implements DiscountRuleInterface
{
    /**
     * Applies the discount rule to an order.
     *
     * @param Order $order
     *
     * @return Amount
     */
    public function apply(Order $order): Amount
    {
        $amount = new Amount(0);

        if ($order->getCustomer()->getRevenue()->getAmount() > 100000) {
            $total = $order->getTotalAfterDiscount()->getAmount();
            $amount = new Amount($total / 10);
        }

        return $amount;
    }

    /**
     * Provides a helpful description of this rule.
     *
     * @return string
     */
    public function description(): string
    {
        return "Discount of 10% on the total order, because the revenue is over 1000 EUR";
    }
}