<?php

namespace App\DiscountRules;

use App\ObjectValues\Amount;
use App\Order;

interface DiscountRuleInterface
{
    public function apply(Order $order): Amount;

    public function description(): string;
}
