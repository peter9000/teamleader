<?php

namespace App\DiscountRules;

use App\ObjectValues\Amount;
use App\Order;

final class TwentyFivePercentOnCheapestToolDiscountRule implements DiscountRuleInterface
{
    /**
     * Applies the discount rule to an order.
     *
     * @param Order $order
     *
     * @return Amount
     */
    public function apply(Order $order): Amount
    {
        $discountValue = 0;

        $products = [];
        $totalNumberOfTools = 0;
        foreach ($order->getOrderLines() as $orderLine) {
            if ($orderLine->getProduct()->getCategory() == 1) {
                $totalNumberOfTools += $orderLine->getQuantity();
                $products[$orderLine->getProduct()->getId()] = $orderLine->getProduct()->getPrice()->getAmount();
            }
        }

        if ($totalNumberOfTools >= 2) {
            // Get the cheapest tool.
            $productIds = array_keys($products, min($products));
            $productId = array_shift($productIds);

            foreach ($order->getOrderLines() as $orderLine) {
                if ($orderLine->getProduct()->getId() == $productId) {
                    $discountValue += $orderLine->getLinePrice()->getAmount() / 100 * 25;
                }
            }
        }

        return new Amount($discountValue);
    }

    /**
     * Provides a helpful description of this rule.
     *
     * @return string
     */
    public function description(): string
    {
        return "Get 25% off on the cheapest 'Tool' products, if you buy two or more.";
    }
}