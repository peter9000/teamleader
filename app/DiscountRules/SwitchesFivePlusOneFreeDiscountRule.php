<?php

namespace App\DiscountRules;

use App\ObjectValues\Amount;
use App\Order;

final class SwitchesFivePlusOneFreeDiscountRule implements DiscountRuleInterface
{
    /**
     * Applies the discount rule to an order.
     *
     * @param Order $order
     *
     * @return Amount
     */
    public function apply(Order $order): Amount
    {
        $discountValue = 0;
        foreach ($order->getOrderLines() as $orderLine) {
            if ($orderLine->getProduct()->getCategory() == 2 && $orderLine->getQuantity() >= 6) {
                $numberOfFreeProducts = (int)floor($orderLine->getQuantity() / 6);
                $discountValue += $numberOfFreeProducts * $orderLine->getUnitPrice()->getAmount();
            }
        }

        return new Amount($discountValue);
    }

    /**
     * Provides a helpful description of this rule.
     *
     * @return string
     */
    public function description(): string
    {
        return "Buy five, get one free for all switches.";
    }
}