<?php

namespace App;

use App\ObjectValues\Amount;

/**
 * Class Product
 *
 * @package App
 */
class Product
{
    /* @var string $id */
    private $id;

    /* @var string $description */
    private $description;

    /* @var int $category */
    private $category;

    /* @var Amount $price */
    private $price;

    /**
     * Product constructor.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->description = $data['description'];
        $this->category = (int)$data['category'];
        $this->price = new Amount($data['price']);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCategory(): int
    {
        return $this->category;
    }

    /**
     * @return Amount
     */
    public function getPrice(): Amount
    {
        return $this->price;
    }
}
