<?php

namespace App\ObjectValues;

/**
 * Class Amount
 *
 * Retains the amount as an int, times one hundred. For example, 29.99 is stored as 2999.
 *
 * @package App\ObjectValues
 */
class Amount
{
    /* @var int $amount */
    private $amount;

    /**
     * Amount constructor.
     *
     * @param string|int|double $amount
     */
    public function __construct($amount)
    {
        if (is_string($amount)) {
            $amount = str_replace('.', '',$amount);
        }

        $this->amount = (int)$amount;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * Formats the amount with the correct decimal notation.
     *
     * @return string
     */
    public function getFormattedAmount()
    {
        return number_format($this->amount / 100, 2, '.', '');
    }
}
